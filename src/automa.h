#ifndef _AUTOMA_H
#define _AUTOMA_H

/*
 * TODO:
 *      - Astrarre funzione update che verrà poi chiamata con un for: NEXT_GENERATION
 */


/*
 * Vicini: matrice 3x3
 * Livelli 5:
 *  [HOT] 1.giallo, 2.arancio, 3.rosso, 4.marrone, 5.nero
 *  [COLD] 1.celeste, 2.azzurro, 3.blu, 4.viola, 5.nero
 * contrassegnati in numero e lettera categoria.
 * ogni cella della matrice gameboard è una struct
 *
 * RULES:
 * - user può mettere solo automi cold or hot solo di livello 1, a bottone via
 *   parte a generare generazioni.
 */

#define GAMEBOARD_HEIGHT 1024
#define GAMEBOARD_WIDTH 1024


 /*
  * struttura cella matrice gameboard
  */
struct cell {
  unsigned short int level;
  char class; //hot or cold
  //int neighbouring; //numero di vicini di 3x3
};



  #endif
