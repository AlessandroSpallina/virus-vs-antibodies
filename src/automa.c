#include "headers.h"
#include "automa.h"


//funzione get3x3 da passare risultato a funzione neighbouring_counter
// ritorna 3x3 con elemento centrale [1,1] elemento importante
struct cell[3][3] get3x3 (struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int i, int j) //coord elemento importante
{
  struct cell matrix[3][3];
  int l, m; //indici gameboard
  int x, y; //indici 3x3

    for(x=0, l=i-1; l<=i+1; x++, j++) {

      for(y=0, m=j-1; m<=j+1; y++, m++) {
        matrix[x][y]=gameboard[l][m];
      }

    }

    return matrix;

}

//funzione conta vicino, singola cell (for per tutta la matrice da fare fuori)
int neighbouring_counter (struct cell matrix[3][3], char class) //'c' o 'h'
{
int i,j;
int counter=0;

  for(i=0; i<3; i++) {

    for(j=0; j<3; j++) {
      if (i==1 && j==1)
        continue;

      if (matrix[i][j].class != class)
        counter--;

      counter++;
    }
  }

  return counter;
}

void neighbouring_steal_level(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int i, int j)
{
  int l=i-1;
  int m=j-1;

  for(; l<i+1; l++)
    for(; m<j+1; m++) {
      if(l==i && m==j)
        continue;

      if(gameboard[l][m].level>0)
        gameboard[l][m].level--;
    }
}

void neighbouring_donate_level(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int i, int j)
{
  int l=i-1;
  int m=j-1;

  for(; l<i+1; l++)
    for(; m<j+1; m++) {
      if(l==i && m==j)
        continue;

      if(gameboard[l][m].level<5)
        gameboard[l][m].level++;
    }


}

//funzione update
void next_generation (struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH])
{
int i,j;

  for(i=0; i<GAMEBOARD_HEIGHT; i++)
    for(j=0; j<GAMEBOARD_WIDTH; j++) {
      //se n vicini è pari
      if(neighbouring_counter(get3x3(gameboard, i, j), gameboard[i][j].class)%2==0) {
        neighbouring_steal_level(gameboard, i, j);
        if(gameboard[i][j].level<5)
          gameboard[i][j].level=5;
      } else {
        //se n vicini è dispari
        neighbouring_donate_level(gameboard, i, j);
        if(gameboard[i][j].level<5)
          gameboard[i][j].level=1;
      }
    }



}
