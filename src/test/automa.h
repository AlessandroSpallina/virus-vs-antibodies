#ifndef _AUTOMA_H
#define _AUTOMA_H

#define GAMEBOARD_LENG 25 //matrice NxN

extern void init_gameboard(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG]);
extern void print_gameboard(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG]);
extern void next_generation(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG]);

#endif
