#include "headers.h"
#include "automa.h"

//funzione conta vicino, singola cell (for per tutta la matrice da fare fuori)
static int neighbouring_counter (struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int py, int px) //'c' o 'h'
{
int i,j;
int counter=0;
struct cell matrix[3][3];

  //printf("%d %d\n", py, px);


  if ((py-1>0 || py+1>GAMEBOARD_HEIGHT) && (px-1>0 || px+1<GAMEBOARD_WIDTH)) {
    printf("\n");
    matrix[0][0].level=gameboard[py-1][px-1].level;
    matrix[0][0].class=gameboard[py-1][px-1].class;
    //printf("%u%c ", matrix[0][0].level, matrix[0][0].class);
    matrix[0][1].level=gameboard[py-1][px].level;
    matrix[0][1].class=gameboard[py-1][px].class;
    //printf("%u%c ", matrix[0][1].level, matrix[0][1].class);
    matrix[0][2].level=gameboard[py-1][px+1].level;
    matrix[0][2].class=gameboard[py-1][px+1].class;
    //printf("%u%c \n", matrix[0][2].level, matrix[0][2].class);

    matrix[1][0].level=gameboard[py][px-1].level;
    matrix[1][0].class=gameboard[py][px-1].class;
    //printf("%u%c ", matrix[1][0].level, matrix[1][0].class);
    matrix[1][1].level=gameboard[py][px].level;
    matrix[1][1].class=gameboard[py][px].class;
    //printf("%u%c ", matrix[1][1].level, matrix[1][1].class);
    matrix[1][2].level=gameboard[py][px+1].level;
    matrix[1][2].class=gameboard[py][px+1].class;
    //printf("%u%c \n", matrix[1][2].level, matrix[1][2].class);

    matrix[2][0].level=gameboard[py+1][px-1].level;
    matrix[2][0].class=gameboard[py+1][px-1].class;
    //printf("%u%c ", matrix[2][0].level, matrix[2][0].class);
    matrix[2][1].level=gameboard[py+1][px].level;
    matrix[2][1].class=gameboard[py+1][px].class;
    //printf("%u%c \n", matrix[2][1].level, matrix[2][1].class);
    matrix[2][2].level=gameboard[py+1][px+1].level;
    matrix[2][2].class=gameboard[py+1][px+1].class;
    //printf("%u%c ", matrix[2][2].level, matrix[2][2].class);

  } else {
    return ((gameboard[py][px].level+7)/2);
  }/*
  printf("\nmatrix: ");
  for(int a=0; a<3; a++)
    for(int b=0; b<3; b++)
      printf("%u", matrix[a][b].level);*/

  for(i=0; i<3; i++) {
    for(j=0; j<3; j++) {
      if (i==1 && j==1)
        continue;
      if (matrix[i][j].class != matrix[1][1].class)
        counter--;
      counter++;
    }
  }

  //printf("counter %d\n", counter);
  return counter;
}

static void neighbouring_steal_level(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int i, int j)
{
  int l=i-1;
  int m=j-1;

  for(; l<i+1; l++)
    for(; m<j+1; m++) {
      if(l==i && m==j)
        continue;

      if(gameboard[l][m].level>0)
        gameboard[l][m].level--;
    }
}

static void neighbouring_donate_level(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH], int i, int j, char class)
{
  int l=i-1;
  int m=j-1;

  for(; l<i+1; l++)
    for(; m<j+1; m++) {
      if(l==i && m==j)
        continue;

      if(gameboard[l][m].level<5) {
        gameboard[l][m].level++;
        //gameboard[l][m].class=class;
      }
    }


}

//funzione update
void next_generation (struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH])
{
int i,j;


  for(i=0; i<GAMEBOARD_HEIGHT; i++) {
    for(j=0; j<GAMEBOARD_WIDTH; j++) {

      //se n vicini è pari
      if(neighbouring_counter(gameboard, i, j)%2==0) {
        neighbouring_steal_level(gameboard, i, j);
        if(gameboard[i][j].level<5)
          gameboard[i][j].level=5;/*
      } else {
        //se n vicini è dispari
        //neighbouring_donate_level(gameboard, i, j, gameboard[i][j].class);
        if(gameboard[i][j].level<5)
          gameboard[i][j].level=1;*/
      }
    }
  }
}

void init_gameboard(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH])
{
int i,j;

for(i=0; i<GAMEBOARD_HEIGHT; i++)
  for(j=0; j<GAMEBOARD_WIDTH; j++) {

    gameboard[i][j].level=0;
    gameboard[i][j].class=' ';
  }

}

void print_gameboard(struct cell gameboard[GAMEBOARD_HEIGHT][GAMEBOARD_WIDTH])
{
int i, j;

  printf("\n");
  for(i=0; i<GAMEBOARD_HEIGHT; i++) {
    for(j=0; j<GAMEBOARD_WIDTH; j++) {
      if(gameboard[i][j].level!=0)
        printf("[%u%c]",gameboard[i][j].level, gameboard[i][j].class);
      else
        printf("[  ]");
    }
    printf("\n\n");
  }
}
