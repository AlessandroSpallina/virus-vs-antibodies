#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "automa.h"

/*
 *  [N to -1] COLD
 *  [1 to N] HOT
 *
 */

void init_gameboard(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG])
{
int i,j;

  for(i=0; i<GAMEBOARD_LENG; i++)
    for(j=0; j<GAMEBOARD_LENG; j++)
      gameboard[i][j]=0;

}

void print_gameboard(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG])
{
int i,j;

  for(i=0; i<GAMEBOARD_LENG; i++) {
    for(j=0; j<GAMEBOARD_LENG; j++) {
      if(gameboard[i][j]!=0) {
        if(gameboard[i][j]>0)
          printf("[%dH]", gameboard[i][j]);
        else
          printf("[%dC]", abs(gameboard[i][j]));
      } else {
          if(gameboard[i][j]==0)
            printf("[  ]");
      }
    }
    printf("\n");
  }
}

int counter_neighbouring(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG], int i, int j)
{
int counter=0;

    counter+=gameboard[i][j];

    if(i-1>=0)
      counter+=gameboard[i-1][j];

    if(i+1<GAMEBOARD_LENG)
      counter+=gameboard[i+1][j];

    if(j-1>=0)
      counter+=gameboard[i][j-1];

    if(j+1<GAMEBOARD_LENG)
      counter+=gameboard[i][j+1];

  return counter;
}

static void donate(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG], int i, int j)
{
int class;

    if(gameboard[i][j]<0)
      class = -1;
    else
      class = 1;

    if(i-1>=0)
      gameboard[i-1][j]+=class;

    if(i+1<GAMEBOARD_LENG)
      gameboard[i+1][j]+=class;

    if(j-1>=0)
      gameboard[i][j-1]+=class;

    if(j+1<GAMEBOARD_LENG)
      gameboard[i][j+1]+=class;

    gameboard[i][j]=0;

}

static void steal(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG], int i, int j)
{
  int class;

      if(gameboard[i][j]<0)
        class = 1;
      else
        class = -1;

      if(i-1>=0)
        gameboard[i-1][j]+=class;

      if(i+1<GAMEBOARD_LENG)
        gameboard[i+1][j]+=class;

      if(j-1>=0)
        gameboard[i][j-1]+=class;

      if(j+1<GAMEBOARD_LENG)
        gameboard[i][j+1]+=class;

      gameboard[i][j]=class*5;

}


void next_generation(int gameboard[GAMEBOARD_LENG][GAMEBOARD_LENG])
{
int a,b;
int counter;

  for(a=0; a<GAMEBOARD_LENG; a++) {
    for(b=0; b<GAMEBOARD_LENG; b++) {
      if(gameboard[a][b]!=0) {
        counter=counter_neighbouring(gameboard, a, b);

        if(counter%2==0)
          donate(gameboard, a, b);
        else
          steal(gameboard, a, b);
      }
    }
  }
}
