#include "sdl_utils.h"
#include "shared.h"
//#include "virus_antibodies_engine.h"

bool   quit   = false;
int    option = 0;

//TODO don't put these params here, use a struct cellular_autonomom ca->param1 ...
float param1;
int param2;
bool enable_param3, status_pause = false;

enum options 
{
   PARAM1 = 0,
   PARAM2,
   PARAM3,
   MAX_OPTIONS 
};

void draw_panel(SDL_Surface *surface, SDL_Rect *dest)
{
    char buffer[32];
    int i = 0;

    struct label
    {
        char *name;
        int  type;
        void *var;
    };

    struct label lbl[] = 
    {
        {"parameter_1", FLOAT, &param1},
        {"parameter_2", INT,   &param2},
        {"parameter_3", BOOL,  &enable_param3},
        {NULL,          0,     NULL}
    };
    
    while (lbl[i].name)
    {
        sprintf(buffer, lbl[i].name);
        if (option == i)
            print_text(surface, dest, buffer, 11,
                       (SDL_Color){220,0,0,70}, 10, 40 + i * 30);
        else
            print_text(surface, dest, buffer, 11,
                       (SDL_Color){220,220,220,0}, 10, 40 + i * 30);

        switch (lbl[i].type)
        {
            case FLOAT: /* float value */
                sprintf(buffer, "> %f          ", *((float*)lbl[i].var));
                break;
            case INT: /* int value */
                sprintf(buffer, "> %d          ", *((int*)lbl[i].var));
                break;
            case BOOL: /* bool value */
                if (*((int*)lbl[i].var) == 1)
                    sprintf(buffer, "> true         ");
                else if (*((int*)lbl[i].var) == 0)
                    sprintf(buffer, "> false        ");
                else if (*((int*)lbl[i].var) == 2)
                    sprintf(buffer, "> AUTO        ");
                else
                    sprintf(buffer, "> NOT VALID   ");
                break;
            default:
                break;
        }
           
        buffer[10] = '\0';
        if (option == i)
            print_text(surface, dest, buffer, 11,
                       (SDL_Color){220,0,0,0}, 10, 52 + i * 30);
        else
            print_text(surface, dest, buffer, 11,
                       (SDL_Color){220,220,220,70}, 10, 52 + i * 30);
        i++;
    }
}

/******************************** THREADS *************************************/

void *event_thread(void *arg)
{
    SDL_Event *event = (SDL_Event*) arg;
    
    while (1)
    {
        SDL_WaitEvent(event); /* SDL_PollEvent(event); */
        
        switch (event->type)
        {
            case SDL_KEYDOWN:
                switch (event->key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    case SDLK_LEFT:
                        //set_val(bds, option, -1);
                        break;
                    case SDLK_RIGHT:
                        //set_val(bds, option, 1);
                        break;
                    case SDLK_SPACE:
                        if (!status_pause)
                        {        
                            status_pause = true;
                        }
                        else
                        {
                            status_pause = false;
                        }
                        break;
                    default:
                        break;

                }
                break;
            case SDL_KEYUP:
                switch (event->key.keysym.sym)
                {
                    case SDLK_UP:
                        option = !option?(MAX_OPTIONS - 1):(option - 1);
                        break;
                    case SDLK_DOWN:
                        option = (option + 1) % MAX_OPTIONS;
                        break;
                    default:
                        break;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                break;
            case SDL_MOUSEBUTTONUP:
                break;
            case SDL_MOUSEMOTION:
                /*if (enable_cursor == 1)
                {
                    x = event->motion.x;
                    y = event->motion.y;
                }*/
                break;  
            case SDL_QUIT:
                quit = true;
                break;
            default:
                break;
        }
        SDL_Delay(1);
    }
    
    pthread_exit(NULL);
}

void *cellaut_thread(void *arg)
{
    //cell *elem = NULL;
    int i;
    
    while (1)
    {
        /*
        for (i = 0; i < cell->size; i++)
        {
            elem = vector_at(elements, i);
            if (elem)
                cellular_autonomom_update(cell);
        }
        */
        SDL_Delay(1);
    }
}

/********************************* MAIN **************************************/

int main(int argc, char *argv[])
{
    SDL_Surface *screen;
    SDL_Surface *background_img, *cell_img;
    SDL_Rect    dest, background_rect, cell_rect;
    SDL_Event event;
    
    int i;
    //cell* ind = NULL;
    
    srand(time(NULL));
    screen = sdl_init();
    
    /* images */
    load_image("art/cell.png", &cell_img, &cell_rect);
    load_image("art/background.jpg", &background_img, &background_rect);

    //if (!(cell = calloc(1, sizeof(cells))));
    //   cellular_autonomom_init();

    /* TODO init all cells */
    
    /* pthread */
    pthread_t      tid;
    pthread_attr_t tattr;
    int ret;

    ret = pthread_attr_init(&tattr);
    ret = pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_JOINABLE);

    if ((ret = pthread_create(&tid, &tattr, event_thread, &event)))
    {
        perror("pthread_create");
        exit(1);
    }

    if ((ret = pthread_create(&tid, &tattr, cellaut_thread, NULL)))
    {
        perror("pthread_create");
        exit(1);
    }

    while (!quit)
    {
        /*
        for (i = 0; i < cells->size; i++)
        {
            elem = vector_at(cells->vct, i);
            if (elem)
            {                
                SDL_BlitSurface(elem->img_cell, &cell_rect, screen, &dest);
            }
        }
        */
        
        /*
        if (enable_cursor != 0)
        {
            SDL_BlitSurface(cursor_img, &cursor_rect, screen, &dest);
        }
        */
        
        print_text(screen, &dest,  PACKAGE_STRING, 14, (SDL_Color){255,255,255,100}, 10, 10);
    
        draw_panel(screen, &dest);
    
        SDL_Flip(screen);
        
        check_fps();
        
        dest.x = 0;
        dest.y = 0; 
        SDL_BlitSurface(background_img, &background_rect, screen, &dest);
    }
    
    SDL_FreeSurface(background_img);
    SDL_FreeSurface(cell_img);
    //SDL_Quit();
    close_font();
    
    return 0;
}
